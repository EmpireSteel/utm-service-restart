#!/bin/bash

sudo systemctl stop egais.service;
sudo systemctl stop pcscd.service;
sudo systemctl kill pcscd.service;
sleep 5;
sudo systemctl start pcscd.service;
sleep 5;
sudo systemctl start egais.service;
