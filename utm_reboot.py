# coding=utf-8
# !/usr/bin/python
#--------------------------------------------------------------------------------------------
# Version  5.3(22.11.2018)
#Author: Gavrilov Andrew
#Email: andrey.gavrilov@x5.ru
#--------------------------------------------------------------------------------------------
import time
from datetime import datetime



def start_time(minutes):
    now = time.time()
    start = now - 60 * minutes
    return start


def reading_error_log(check_timer):
    start = start_time(check_timer)
    try:
        with open("/var/log/zabbix/utm_ecount.log", 'r') as f:
            line = f.readline().split()
            # Счетчик ошибок
            error_count = int(line[0])
    except IOError:
        # Счетчик ошибок
        error_count = 0
    return start, error_count


def error_finder(start, log_files, errors):
    for log in log_files:
        find = 0
        with open(log, 'r') as f:
            for line in f.readlines():
                if find == 0:
                    try:
                        date = time.mktime(datetime.strptime(line[:19], '%Y-%m-%d %H:%M:%S').timetuple())
                        if date >= start:
                            find = 1
                        else:
                            continue
                    except:
                        continue
                if find == 1:
                    for error in errors:
                        if error in line:
                            return error
    return 'OK'


def reboot(error_count, finder_result):
    if finder_result == 'OK':
        with open("/var/log/zabbix/utm_ecount.log", 'w') as f:
            f.write(str(0))
        log_form(finder_result)
    else:
        error_count += 1
        with open("/var/log/zabbix/utm_ecount.log", 'w') as f:
            f.write(str(error_count))
        log_form(finder_result)


def log_form(status):
    restart_count = 0
    try:
        with open("/var/log/zabbix/utm_reboot.log", 'r') as f:
            log_line = f.readlines()[-1].split()
            counter = int(log_line[2])
            now = str(datetime.now())[:10]
            if now[5:7] == '01' and log_line[5:7] != '01':
                counter = 0
            if status == "OK":
                restart_count = counter
            else:
                restart_count = counter + 1
            if now == log_line[0]:
                update = False
            else:
                update = True
    except:
        update = True
    if update:
        with open("/var/log/zabbix/utm_reboot.log", 'w') as f:
            f.write(str(datetime.now())[:19] + " " + str(restart_count) + " [" + status + "]\n")
    else:
        with open("/var/log/zabbix/utm_reboot.log", 'a') as f:
            f.write(str(datetime.now())[:19] + " " + str(restart_count) + " [" + status + "]\n")


if __name__ == "__main__":
    # Временной интервал проверяемый скриптом
    check_timer = 3
    # Пути к файлам
    log_file = ["/opt/utm/transport/l/transport_info.log", "/opt/utm/updater/l/update.log"]
    # Список ошибок
    error_list = ["CKR_"]
    # Проверка логов
    start, error_count = reading_error_log(check_timer)
    # Поиск ошибок
    finder_result = error_finder(start, log_file, error_list)
    reboot(error_count, finder_result)
    print "Done"
